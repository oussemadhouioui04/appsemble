import { type ReactElement } from 'react';

import MDXContent from './index.md';

export function IndexPage(): ReactElement {
  return <MDXContent />;
}
