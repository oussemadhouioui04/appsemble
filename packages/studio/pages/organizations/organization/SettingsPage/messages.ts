import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  name: {
    id: 'studio.HAlOn1',
    defaultMessage: 'Name',
  },
  nameDescription: {
    id: 'studio.xzWgp6',
    defaultMessage: 'The display name of the organization',
  },
  logo: {
    id: 'studio.k81S1y',
    defaultMessage: 'Logo',
  },
  logoDescription: {
    id: 'studio.QLD5P4',
    defaultMessage: 'The logo that represents this organization',
  },
  selectFile: {
    id: 'studio.G5EJ8b',
    defaultMessage: 'Select logo',
  },
  title: {
    id: 'studio.V0yq88',
    defaultMessage: 'Settings {name}',
  },
  submit: {
    id: 'studio.wSZR47',
    defaultMessage: 'Submit',
  },
  settings: {
    id: 'studio.D3idYv',
    defaultMessage: 'Settings',
  },
  email: {
    id: 'studio.sy+pv5',
    defaultMessage: 'Email',
  },
  emailDescription: {
    id: 'studio.UHheiE',
    defaultMessage: 'The email address users can use to contact this organization.',
  },
  description: {
    id: 'studio.Q8Qw5B',
    defaultMessage: 'Description',
  },
  descriptionDescription: {
    id: 'studio./9RhC2',
    defaultMessage: 'A short description about the organization',
  },
  website: {
    id: 'studio.JkLHGw',
    defaultMessage: 'Website',
  },
  websiteDescription: {
    id: 'studio.9rXRSk',
    defaultMessage: 'The website of your organization.',
  },
});
