import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: {
    id: 'studio.JD2pwH',
    defaultMessage: 'Organizations',
  },
  description: {
    id: 'studio.rgudrj',
    defaultMessage: 'Browse the different organizations who are using Appsemble',
  },
  CreateApps: {
    id: 'studio.na98ss',
    defaultMessage: 'The permission to create new apps or copy them from templates.',
  },
  DeleteApps: {
    id: 'studio.jNtX+C',
    defaultMessage: 'The permission to delete apps.',
  },
  EditAppMessages: {
    id: 'studio.773Bdo',
    defaultMessage: 'The permission to edit the app’s translations.',
  },
  EditAppSettings: {
    id: 'studio.84sYlI',
    defaultMessage: 'The permission to edit the app’s settings.',
  },
  EditApps: {
    id: 'studio.1qiYs4',
    defaultMessage: 'The permission to edit the app definition.',
  },
  EditOrganization: {
    id: 'studio.K0PWHi',
    defaultMessage: 'The permission to edit the name and logo of an organization.',
  },
  InviteMember: {
    id: 'studio.4SSVOk',
    defaultMessage:
      'The permission to invite new members into an organization, removing existing invites, and resending invites.',
  },
  ManageAssets: {
    id: 'studio.K3Oxal',
    defaultMessage: 'The permission to create and delete assets.',
  },
  ManageMembers: {
    id: 'studio./BTTpN',
    defaultMessage: 'The permission to remove organization members.',
  },
  ManageResources: {
    id: 'studio.8B+epK',
    defaultMessage: 'The permission to create, edit, and delete resources.',
  },
  ManageRoles: {
    id: 'studio.o+nqjA',
    defaultMessage: 'The permission to change the roles of organization members.',
  },
  ManageTeams: {
    id: 'studio.kyHYHZ',
    defaultMessage: 'The permission to create and delete teams and manage its members.',
  },
  PublishBlocks: {
    id: 'studio.Z8Fm9O',
    defaultMessage: 'The permission to publish blocks for an organization.',
  },
  PushNotifications: {
    id: 'studio.+7+1T0',
    defaultMessage: 'The permission to send manual push notifications for an app.',
  },
  ReadAssets: {
    id: 'studio.GUT+Tf',
    defaultMessage: 'The permission to read assets.',
  },
  ReadResources: {
    id: 'studio.Mz+9Xq',
    defaultMessage: 'The permission to read resources.',
  },
  ViewApps: {
    id: 'studio.uzN4CO',
    defaultMessage: 'The permission to view private apps of an organization.',
  },
  ViewMembers: {
    id: 'studio.BzTzOo',
    defaultMessage: 'The permission to view the list of members in an organization.',
  },
});
