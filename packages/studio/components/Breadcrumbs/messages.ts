import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  breadcrumbs: {
    id: 'studio.ByoZDD',
    defaultMessage: 'Breadcrumbs',
  },
  switchToGuiEditor: {
    id: 'studio.zoSzG+',
    defaultMessage: 'Switch to GUI Editor',
  },
  switchToCodeEditor: {
    id: 'studio.7Znis6',
    defaultMessage: 'Switch to Code Editor',
  },
});
